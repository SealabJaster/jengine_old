﻿module jengine.tests.t01;

version(unittest)
{
    import std.stdio;
    import derelict.sdl2.sdl;
    import jengine.core.sdl, jengine.core.timer, jengine.graphics.window;

    void t01()
    {
        Window.create("Test 01", Rectangle(SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 860, 720), FPS(10));

        Timer timer = Timer(true);
        while(timer.asMilliseconds < 5000)
        {
            SDL_Delay(1000);
            writeln(timer.asMilliseconds);
        }

        Window.destroy();
    }
}