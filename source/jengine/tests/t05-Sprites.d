﻿module jengine.tests.t05;

version(unittest)
{
    import std.experimental.logger;
    import jengine.core.allocators, jengine.core.sdl, jengine.graphics.renderer, jengine.graphics.texture, jengine.graphics.window,
        jengine.core.component, jengine.core.gamestate;

    void t05()
    {
        Window.create("Test 04", Rectangle(SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 860, 720), FPS(60));
        scope(exit) Window.destroy();
        
        auto app = Allocators.GameState.make!Game;
        
        Window.startGameLoop(app);
    }

    class Game : GameState
    {
        private
        {
            struct Output
            {
                void put(string s)
                {
                    info(s);
                }
            }

            Texture tahn;
            Sprite  tahnSprite;
            Sprite  tahnSprite2;
            FPSReporter!Output fps;
        }
        
        public override
        {
            /++
             + Called whenever this GameState is:
             +  Pushed onto the top of the stack.
             +  Popped off of the top of the stack.
             +  Is on the top of the stack, but another GameState is pushed on top of it.
             + 
             + Parameters:
             +  action = The action that took place.
             + ++/
            void onSwap(SwapAction action){}

            /++
             + Called when any data of the component should be loaded.
             + ++/
            void onInit()
            {
                tahn        = Texture.fromFile("Data/Tahn.png\0");
                tahnSprite  = Sprite.make(tahn, Vector2f(0,   360));
                tahnSprite2 = Sprite.make(tahn, Vector2f(360, 0));
                fps         = theAllocator.make!(FPSReporter!Output)(Output());
                fps.onInit();

                super.onInit();
            }
            
            /++
             + Called everytime the component is updated.
             + 
             + Parameters:
             +  gameTime = A reference to the amount of time the last frame took.
             + ++/
            void onUpdate(ref float gameTime)
            {
                fps.onUpdate(gameTime);

                // The sprite's angle can be used in both degrees and radians. But Radians requires a conversion each time.
                tahnSprite.angleAsDegrees = tahnSprite.angleAsDegrees + gameTime; // This should rotate it a little
                tahnSprite.angleAsRadians = tahnSprite.angleAsRadians * 1.25;     // This makes it rotate faster depending on how much it's already rotated by.

                // They both work... hopefully
                if(tahnSprite.angleAsDegrees > 360)
                    tahnSprite.angleAsRadians = 0;

                tahnSprite.position += Vector2f(100.0f * gameTime, 0);
                tahnSprite.colour   += Colour(16, 8, 2, 0);

                if(tahnSprite.position.x > 860.0f)
                    tahnSprite.position.x = 0;

                Vector2i pos;
                SDL_GetMouseState(&pos.x, &pos.y);

                tahnSprite2.rotateTo(pos);
                tahnSprite2.moveInDirection(100.0f * gameTime);

                Renderer.drawSprite(tahnSprite2);
                Renderer.drawSprite(tahnSprite);
            }
            
            /++
             + Called everytime an event is to be processed.
             + 
             + Parameters:
             +  event = The event to process.
             +  handled = Has the event already been handled?
             + 
             + Returns:
             +  $(D true) if $(B event) was handled. $(D false) otherwise.
             + ++/
            bool onEvent(SDL_Event event, bool handled)
            {
                return false;
            }
            
            /++
             + Called when any data of the component should be unloaded.
             + ++/
            void onUninit()
            {
                Texture.dispose(tahn);
                theAllocator.dispose(fps);
                Sprite.dispose(tahnSprite);
                Sprite.dispose(tahnSprite2);

                super.onUninit();
            }
        }
    }
}