﻿module jengine.tests.t07;

version(unittest)
{
    import std.stdio, std.experimental.logger;
    import jengine.core.allocators, jengine.core.timer, jengine.core.component, jengine.core.sdl, jengine.core.world, jengine.core.gamestate;
    import jengine.graphics.window, jengine.graphics.renderer, jengine.graphics.texture, jengine.graphics.cache, jengine.graphics.font, jengine.graphics.camera;

    private
    {
        enum PLAYER_LIVES       = 3;
        enum PLAYER_SPEED       = 100.0f;
        enum ZOMBIE_SPEED       = 130.0f;
        enum BULLET_SPEED       = 150.0f;
        enum ZOMBIE_SPAWN       = 450;      // In ticks
        enum GUN_RELOAD         = 400;      // In ticks

        Texture Tahn;
        Font    Arial;
    }

    void t07()
    {
        Window.create("Test 07", Rectangle(SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 860, 720), FPS(60));
        scope(exit) Window.destroy();
        
        auto app = Allocators.GameState.make!Game;
        scope(exit) Cache.dispose();

        PhysicalWorld.setup();
        scope(exit) PhysicalWorld.destroy();
        
        Window.startGameLoop(app);
    }
    
    class Game : GameState
    {
        private
        {
            struct Output
            {
                void put(string s)
                {
                    info(s);
                }
            }

            Timer   _timer;
            FPSReporter!Output _fps;
        }
        
        public override
        {
            /++
             + Called whenever this GameState is:
             +  Pushed onto the top of the stack.
             +  Popped off of the top of the stack.
             +  Is on the top of the stack, but another GameState is pushed on top of it.
             + 
             + Parameters:
             +  action = The action that took place.
             + ++/
            void onSwap(SwapAction action){}

            /++
             + Called when any data of the component should be loaded.
             + ++/
            void onInit()
            {
                Tahn  = Cache.loadTexture("Data/Tahn.png");
                Arial = Cache.loadFont("Data/Arial.ttf", 24);

                PhysicalWorld.add(theAllocator.make!Player);
                PhysicalWorld.add(theAllocator.make!Gun);

                this._fps = theAllocator.make!(FPSReporter!Output)(Output());
                this._fps.onInit();

                this._timer = Timer(true);
                super.onInit();
            }
            
            /++
             + Called everytime the component is updated.
             + 
             + Parameters:
             +  gameTime = A reference to the amount of time the last frame took.
             + ++/
            void onUpdate(ref float gameTime)
            {
                PhysicalWorld.onUpdate(gameTime);

                // Spawn a zombie every 0.35 seconds.
                if(this._timer.ticks >= ZOMBIE_SPAWN)
                {
                    PhysicalWorld.add(theAllocator.make!Zombie);
                    this._timer.restart;
                }

                this._fps.onUpdate(gameTime);
            }
            
            /++
             + Called everytime an event is to be processed.
             + 
             + Parameters:
             +  event = The event to process.
             +  handled = Has the event already been handled?
             + 
             + Returns:
             +  $(D true) if $(B event) was handled. $(D false) otherwise.
             + ++/
            bool onEvent(SDL_Event event, bool handled)
            {
                return PhysicalWorld.onEvent(event, handled) || handled;
            }
            
            /++
             + Called when any data of the component should be unloaded.
             + ++/
            void onUninit()
            {
                PhysicalWorld.destroy();
                theAllocator.dispose(this._fps);

                super.onUninit();
            }
        }
    }

    class Player : PhysicalComponent
    {
        private
        {
            ubyte   _lives;
            bool    _up, _left, _right, _down;
            Text    _lifeText;
        }

        public
        {
            this()
            {
                this._lives = PLAYER_LIVES;
            }

            override
            {
                /++
                 + Called when any data of the component should be loaded.
                 + ++/
                void onInit()
                {
                    this._lifeText          = Text.make("Lives: 3", Arial);
                    this._lifeText.colour   = Renderer.black;

                    this.sprite = Sprite.make(Tahn, Vector2f((860 / 2) - 16, (720 / 2) - 16));
                    this.name   = "Player";

                    this.sprite.camera = Camera.instance;
                    super.onInit();
                }
                
                /++
                 + Called everytime the component is updated.
                 + 
                 + Parameters:
                 +  gameTime = A reference to the amount of time the last frame took.
                 + ++/
                void onUpdate(ref float gameTime)
                {
                    if(this._up)
                        this.sprite.position += Vector2f(0, -PLAYER_SPEED * gameTime);
                    if(this._left)
                        this.sprite.position += Vector2f(-PLAYER_SPEED * gameTime, 0);
                    if(this._right)
                        this.sprite.position += Vector2f(PLAYER_SPEED * gameTime, 0);
                    if(this._down)
                        this.sprite.position += Vector2f(0, PLAYER_SPEED * gameTime);

                    Renderer.drawText(this._lifeText);
                }
                
                /++
                 + Called everytime an event is to be processed.
                 + 
                 + Parameters:
                 +  event = The event to process.
                 +  handled = Has the event already been handled?
                 + 
                 + Returns:
                 +  $(D true) if $(B event) was handled. $(D false) otherwise.
                 + ++/
                bool onEvent(SDL_Event event, bool handled)
                {
                    void setFlag(ref bool flag, SDL_Keycode code)
                    {
                        if(event.type == SDL_KEYDOWN && event.key.keysym.sym == code)
                        {
                            flag    = true;
                            handled = true;
                        }
                        else if(event.type == SDL_KEYUP && event.key.keysym.sym == code)
                            flag = false;
                    }

                    setFlag(this._up,           SDLK_UP);
                    setFlag(this._down,         SDLK_DOWN);
                    setFlag(this._left,         SDLK_LEFT);
                    setFlag(this._right,        SDLK_RIGHT);

                    return handled;
                }
                
                /++
                 + Called when any data of the component should be unloaded.
                 + ++/
                void onUninit()
                {
                    Text.dispose(this._lifeText);
                    super.onUninit();
                }

                /++
                 + Called whenever the physical component overlaps with another.
                 + 
                 + Parameters:
                 +  other = The other entity.
                 + ++/
                void onHitEntity(PhysicalComponent other)
                {
                    if(cast(Zombie)other)
                    {
                        this._lives -= 1;
                        PhysicalWorld.remove(other);

                        import std.conv : to;
                        this._lifeText.text = "Lives: " ~ this._lives.to!string;

                        if(this._lives == 0)
                            throw new NoobException("How could you be such a noob!? (To clarify, this is a joke)");
                    }
                }
                class NoobException : Exception
                {
                    this(string message)
                    {
                        super(message);
                    }
                }
            }
        }
    }

    class Bullet : PhysicalComponent
    {
        private
        {
            float f;
        }
        
        public
        {
            this(float f)
            {
                this.f = f;
            }

            override
            {
                /++
                 + Called when any data of the component should be loaded.
                 + ++/
                void onInit()
                {
                    this.sprite = Sprite.make(Tahn, Vector2f(), BoundingType.Circle);
                    this.sprite.colour          = Renderer.blue;
                    this.sprite.position        = PhysicalWorld.find("Player").sprite.position;
                    this.sprite.angleAsDegrees  = this.f;
                    this.sprite.camera          = Camera.instance;

                    super.onInit();
                }
                
                /++
                 + Called everytime the component is updated.
                 + 
                 + Parameters:
                 +  gameTime = A reference to the amount of time the last frame took.
                 + ++/
                void onUpdate(ref float gameTime)
                {
                    this.sprite.moveInDirection(BULLET_SPEED * gameTime);

                    if(!this.sprite.isOnScreen)
                        PhysicalWorld.remove(this);
                }
                
                /++
                 + Called everytime an event is to be processed.
                 + 
                 + Parameters:
                 +  event = The event to process.
                 +  handled = Has the event already been handled?
                 + 
                 + Returns:
                 +  $(D true) if $(B event) was handled. $(D false) otherwise.
                 + ++/
                bool onEvent(SDL_Event event, bool handled)
                {
                    return handled;
                }
                
                /++
                 + Called when any data of the component should be unloaded.
                 + ++/
                void onUninit()
                {
                    super.onUninit();
                }
                
                /++
                 + Called whenever the physical component overlaps with another.
                 + 
                 + Parameters:
                 +  other = The other entity.
                 + ++/
                void onHitEntity(PhysicalComponent other)
                {
                }
            }
        }
    }

    class Gun : PhysicalComponent
    {
        private
        {
            bool _fire;
            Timer _timer;
        }
        
        public
        {
            override
            {
                /++
                 + Called when any data of the component should be loaded.
                 + ++/
                void onInit()
                {
                    this.sprite = Animation.make(Cache.loadTexture("Data/TahnAni.png"), AnimationInfo(32, 32, 0.50f, true));
                    this.animation.start();

                    this.sprite.colour = Renderer.gray;
                    this.sprite.centre = Vector2i(-24, 16);
                    this.sprite.camera = Camera.instance;

                    this._timer.start;
                    super.onInit();
                }
                
                /++
                 + Called everytime the component is updated.
                 + 
                 + Parameters:
                 +  gameTime = A reference to the amount of time the last frame took.
                 + ++/
                void onUpdate(ref float gameTime)
                {
                    // Move the gun to be in position of the player.
                    auto player = PhysicalWorld.find("Player");
                    assert(player !is null);

                    Vector2i pos = this.sprite.camera.toWorldCoords(InputManager.mousePosition.asVector!float).asVector!int;

                    this.sprite.rotateTo(pos);
                    this.sprite.position = player.sprite.position + Vector2f(player.sprite.texture.size.x + 8, 0);

                    if(this._fire)
                    {
                        this._fire = false;

                        if(this._timer.ticks >= GUN_RELOAD)
                        {
                            PhysicalWorld.add(theAllocator.make!Bullet(this.sprite.angleAsDegrees));
                            this._timer.restart;
                        }
                    }

                    this.animation.onUpdate();
                }
                
                /++
                 + Called everytime an event is to be processed.
                 + 
                 + Parameters:
                 +  event = The event to process.
                 +  handled = Has the event already been handled?
                 + 
                 + Returns:
                 +  $(D true) if $(B event) was handled. $(D false) otherwise.
                 + ++/
                bool onEvent(SDL_Event event, bool handled)
                {
                    if(event.type == SDL_MOUSEBUTTONDOWN && event.button.button == SDL_BUTTON_LEFT)
                    {
                        this._fire = true;
                        handled = true;
                    }

                    return handled;
                }
                
                /++
                 + Called when any data of the component should be unloaded.
                 + ++/
                void onUninit()
                {
                    super.onUninit();
                }
                
                /++
                 + Called whenever the physical component overlaps with another.
                 + 
                 + Parameters:
                 +  other = The other entity.
                 + ++/
                void onHitEntity(PhysicalComponent other)
                {
                }
            }
        }
    }

    class Zombie : PhysicalComponent
    {
        private
        {
        }
        
        public
        {
            override
            {
                /++
                 + Called when any data of the component should be loaded.
                 + ++/
                void onInit()
                {
                    import std.random;

                    this.sprite = Sprite.make(Tahn, Vector2f(uniform(0.0f, 720.0f), -32), BoundingType.Circle);
                    this.sprite.colour = Renderer.green;
                    this.sprite.camera = Camera.instance;
                    super.onInit();
                }
                
                /++
                 + Called everytime the component is updated.
                 + 
                 + Parameters:
                 +  gameTime = A reference to the amount of time the last frame took.
                 + ++/
                void onUpdate(ref float gameTime)
                {
                    auto player = PhysicalWorld.find("Player");
                    assert(player !is null);

                    this.sprite.rotateTo(Vector2f(player.sprite.position.x + player.sprite.centre.x, player.sprite.position.y + player.sprite.centre.y).asVector!int);
                    this.sprite.moveInDirection(ZOMBIE_SPEED * gameTime);
                }
                
                /++
                 + Called everytime an event is to be processed.
                 + 
                 + Parameters:
                 +  event = The event to process.
                 +  handled = Has the event already been handled?
                 + 
                 + Returns:
                 +  $(D true) if $(B event) was handled. $(D false) otherwise.
                 + ++/
                bool onEvent(SDL_Event event, bool handled)
                {
                    return handled;
                }
                
                /++
                 + Called when any data of the component should be unloaded.
                 + ++/
                void onUninit()
                {
                    super.onUninit();
                }
                
                /++
                 + Called whenever the physical component overlaps with another.
                 + 
                 + Parameters:
                 +  other = The other entity.
                 + ++/
                void onHitEntity(PhysicalComponent other)
                {
                    if(cast(Bullet)other !is null)
                    {
                        PhysicalWorld.remove(this);
                        PhysicalWorld.remove(other);
                    }
                }
            }
        }
    }

}