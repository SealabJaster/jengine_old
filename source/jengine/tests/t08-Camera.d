﻿module jengine.tests.t08;

version(unittest)
{
    import std.stdio, std.experimental.logger;
    import jengine.core.allocators, jengine.core.timer, jengine.core.component, jengine.core.sdl, jengine.core.world, jengine.core.gamestate;
    import jengine.graphics.window, jengine.graphics.renderer, jengine.graphics.texture, jengine.graphics.camera;
    
    void t08()
    {
        Window.create("Test 08", Rectangle(SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 860, 720), FPS(60));
        scope(exit) Window.destroy();
        
        auto app = Allocators.GameState.make!Game;
        
        PhysicalWorld.setup();
        scope(exit) PhysicalWorld.destroy();
        
        Window.startGameLoop(app);
    }

    private Texture Tahn;
    class Test : PhysicalComponent
    {        
        public override
        {
            /++
             + Called when any data of the component should be loaded.
             + ++/
            void onInit()
            {
                this.sprite = Sprite.make(Tahn);
                this.sprite.camera = Camera.instance;
                
                super.onInit();
            }
            
            /++
             + Called everytime the component is updated.
             + 
             + Parameters:
             +  gameTime = A reference to the amount of time the last frame took.
             + ++/
            void onUpdate(ref float gameTime)
            {
            }
            
            /++
             + Called everytime an event is to be processed.
             + 
             + Parameters:
             +  event = The event to process.
             +  handled = Has the event already been handled?
             + 
             + Returns:
             +  $(D true) if $(B event) was handled. $(D false) otherwise.
             + ++/
            bool onEvent(SDL_Event event, bool handled)
            {
                if(event.type == SDL_MOUSEBUTTONDOWN)
                {
                    auto pos = Camera.instance.toWorldCoords(InputManager.mousePosition.asVector!float);

                    if(this.sprite.box.hasPoint(pos.asVector!int))
                        writeln("Quack");
                }

                return handled;
            }
            
            /++
             + Called when any data of the component should be unloaded.
             + ++/
            void onUninit()
            {
                super.onUninit();
            }
            
            /++
             + Called whenever the physical component overlaps with another.
             + 
             + Parameters:
             +  other = The other entity.
             + ++/
            void onHitEntity(PhysicalComponent other)
            {
            }
        }
    }
    
    class Game : GameState
    {
        private
        {
        }
        
        public override
        {
            /++
             + Called whenever this GameState is:
             +  Pushed onto the top of the stack.
             +  Popped off of the top of the stack.
             +  Is on the top of the stack, but another GameState is pushed on top of it.
             + 
             + Parameters:
             +  action = The action that took place.
             + ++/
            void onSwap(SwapAction action){}
            
            /++
             + Called when any data of the component should be loaded.
             + ++/
            void onInit()
            {
                Tahn = Texture.fromFile("Data/Tahn.png");
                PhysicalWorld.add(theAllocator.make!Test(), "Thing");
                
                super.onInit();
            }
            
            /++
             + Called everytime the component is updated.
             + 
             + Parameters:
             +  gameTime = A reference to the amount of time the last frame took.
             + ++/
            void onUpdate(ref float gameTime)
            {
                enum Speed = 200.0f;
                auto speed = Speed * gameTime;

                if(InputManager.isKeyDown(SDL_SCANCODE_RIGHT))
                    Camera.instance.move(Vector2f(speed, 0));
                if(InputManager.isKeyDown(SDL_SCANCODE_LEFT))
                    Camera.instance.move(Vector2f(-speed, 0));
                if(InputManager.isKeyDown(SDL_SCANCODE_UP))
                    Camera.instance.move(Vector2f(0, -speed));
                if(InputManager.isKeyDown(SDL_SCANCODE_DOWN))
                    Camera.instance.move(Vector2f(0, speed));

                writefln("MouseScreenCoords: %s | MouseWorldCoords: %s", InputManager.mousePosition, 
                    Camera.instance.toWorldCoords(InputManager.mousePosition.asVector!float));

                if(InputManager.isKeyPressed(SDL_SCANCODE_SPACE))
                {
                    auto thing = PhysicalWorld.find("Thing");
                    Camera.instance.centreOn(thing.sprite);
                }

                PhysicalWorld.onUpdate(gameTime);
            }
            
            /++
             + Called everytime an event is to be processed.
             + 
             + Parameters:
             +  event = The event to process.
             +  handled = Has the event already been handled?
             + 
             + Returns:
             +  $(D true) if $(B event) was handled. $(D false) otherwise.
             + ++/
            bool onEvent(SDL_Event event, bool handled)
            {
                return PhysicalWorld.onEvent(event, handled);
            }
            
            /++
             + Called when any data of the component should be unloaded.
             + ++/
            void onUninit()
            {
                PhysicalWorld.destroy();
                Texture.dispose(Tahn);
                super.onUninit();
            }
        }
    }
}