﻿module jengine.tests.t03;

version(unittest)
{
    import std.stdio;
    import jengine.core.allocators, jengine.core.timer, jengine.core.component, jengine.core.sdl, jengine.core.gamestate;
    import jengine.graphics.window;

    void t03()
    {
        Window.create("Test 03", Rectangle(SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 860, 720), FPS(60));
        scope(exit) Window.destroy();

        auto app = Allocators.GameState.make!Game;

        Window.startGameLoop(app);
    }

    class Game : GameState
    {
        private
        {
        }

        public override
        {
            /++
             + Called whenever this GameState is:
             +  Pushed onto the top of the stack.
             +  Popped off of the top of the stack.
             +  Is on the top of the stack, but another GameState is pushed on top of it.
             + 
             + Parameters:
             +  action = The action that took place.
             + ++/
            void onSwap(SwapAction action){}

            /++
             + Called when any data of the component should be loaded.
             + ++/
            void onInit()
            {
                super.onInit();
            }
            float x = 0;
            /++
             + Called everytime the component is updated.
             + 
             + Parameters:
             +  gameTime = A reference to the amount of time the last frame took.
             + ++/
            void onUpdate(ref float gameTime)
            {
                import jengine.graphics.renderer;
                Renderer.drawRect(
                    Rectangle(200, 100, 50, 50),
                    Renderer.red,
                    2,
                    Renderer.black
                    );

                Renderer.drawRect(
                    Rectangle(600, 100, 250, 150),
                    Renderer.blue,
                    8,
                    Renderer.gray
                    );

                Renderer.drawCircle(
                    Circle(40, Vector2f(800, 50)),
                    Renderer.black
                    );

                x += 100.0f * gameTime;
                const sprite = RectangleSprite(Rectangle(cast(int)x, 400, 80, 40), Renderer.cyan);
                Renderer.drawRect(sprite);
            }
            
            /++
             + Called everytime an event is to be processed.
             + 
             + Parameters:
             +  event = The event to process.
             +  handled = Has the event already been handled?
             + 
             + Returns:
             +  $(D true) if $(B event) was handled. $(D false) otherwise.
             + ++/
            bool onEvent(SDL_Event event, bool handled)
            {
                return false;
            }
            
            /++
             + Called when any data of the component should be unloaded.
             + ++/
            void onUninit()
            {
                super.onUninit();
            }
        }
    }
}