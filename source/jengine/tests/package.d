﻿module jengine.tests;

unittest
{
    import derelict.sdl2.sdl, derelict.sdl2.image;
    import std.path, std.file, std.stdio, std.format, std.string, std.conv, std.exception;
    import jengine.core.allocators;

    Allocators.setup();
    assert(Allocators.Default !is null);
    theAllocator = Allocators.Default;

    // Make sure we're in the "lib" folder.
    if(!getcwd().endsWith("lib"))
    {
        if(!exists("lib"))
        {
            writeln("Please run the tests in either the project's root directory, or from the 'lib' folder.");
            return;
        }
        else
            chdir("lib");
    }

	import jengine.core.sdl;
    loadSDL();

    /// Describes a test
    alias TestFunc = void function();
    struct Test
    {
        /// The name of the test
        string name;

        /// The test's function.
        TestFunc func;
    }

    import jengine.tests.t01, jengine.tests.t02, jengine.tests.t03, jengine.tests.t04, jengine.tests.t05, jengine.tests.t06,
        jengine.tests.t07, jengine.tests.t08, jengine.tests.t09;
    Test[] tests = 
    [
        Test("Basic Window Creation and a Timer",           &t01),
        Test("Game loop test",                              &t02),
        Test("Renderer Drawing Test",                       &t03),
        Test("Texture loading and drawing",                 &t04),
        Test("Sprites",                                     &t05),
        Test("Physical world",                              &t06),
        Test("Zombie Shooter",                              &t07),
        Test("Camera Test",                                 &t08),
        Test("Animation Test",                              &t09)
    ];

    // Present all options to the user
    foreach(i, test; tests)
        writefln("%s. %s", i+1, test.name);

    // Formatting
    writeln();

    // Get what test to run
    TestFunc func;
    try
    {
        write("Enter the number of what test to run: ");
        auto option = readln().chomp().to!size_t-1;

        if(option >= tests.length)
            throw new Exception("");

        func = tests[option].func;
    }
    catch(Exception ex)
    {
        writeln("Invalid option");
        return;
    }

    func();
    Allocators.printStats();
}