﻿/++
 + This module contains some pre-defined allocators to be used.
 + ++/
module jengine.core.allocators;

public
{
    import std.experimental.allocator.mallocator, std.experimental.allocator.building_blocks.stats_collector,
        std.experimental.allocator;
}

/++
 + Contains some pre-defined allocators.
 + ++/
static class Allocators
{
    public static
    {
        /// The default allocator for use
        alias Default  = Allocators.statMallocator;
        alias DefaultT = typeof(Default);

        /// The Allocator you should use for allocating GameStates, and the GameStateManager
        alias GameState = Allocators.statMallocator;

        /// The Allocator you should use for allocating Textures.
        alias Texture   = Allocators.statMallocator;

        /// The Allocator you should use for allocating Sprites.
        alias Sprite    = Allocators.statMallocator;

        /// The Allocator you should use for allocating Fonts.
        alias Font      = Allocators.statMallocator;

        /// The Allocator you should use for allocating Texts.
        alias Text      = Allocators.statMallocator;

        alias StatMallocatorT = StatsCollector!(Mallocator, Options.all);

        /++
         + A Mallocator wrapped inside a StatCollector.
         + +/
        CAllocatorImpl!StatMallocatorT  statMallocator;

        /++
         + Setup all of the allocators.
         + ++/
        void setup()
        {
            Allocators.statMallocator = allocatorObject(StatMallocatorT());
        }

        /++
         + Checks to see if $(B theAllocator) is one of the Allocator class' allocators that makes use of $(B StatsCollector)
         + If it is, then it's memory usage is printed out.
         + ++/
        void printStats()
        {
            auto alloc = cast(typeof(Allocators.statMallocator))theAllocator;

            if(alloc !is null)
                alloc.impl.reportStatistics(std.stdio.stdout);
        }
    }
}