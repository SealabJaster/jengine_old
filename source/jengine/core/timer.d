﻿module jengine.core.timer;

private
{
    import derelict.sdl2.sdl : SDL_GetTicks;
}

/++
 + A struct that can be used to take a measurement of time.
 + Based off SDL_GetTicks.
 + ++/
struct Timer
{
    private
    {
        uint    _startTime;
        uint    _stoppedTime;
        bool    _started = false;
    }
    
    @nogc @trusted
    public nothrow
    {
        /++
         + Constructs the timer.
         + 
         + Parameters:
         +  autoStart = If true, will start the timer automatically.
         + ++/
        this(bool autoStart)
        {
            if(autoStart)
                this.start();
        }

        /++
         + Starts the timer.
         ++/
        void start()
        {
            this._startTime     = SDL_GetTicks();
            this._stoppedTime   = this._startTime;
            this._started       = true;
        }
        
        /++
         + Stops the timer.
         ++/
        void stop()
        {
            this._stoppedTime   = SDL_GetTicks();
            this._started       = false;
        }
        
        /++
         + Restarts the timer.
         ++/
        alias restart = start;
        
        /++
         + Resumes the timer if it has been stopped
         ++/
        void resume()
        {
            if(!this._started)
            {
                this._startTime     = (SDL_GetTicks() - this.ticks);
                this._stoppedTime   = this._startTime;
                this._started       = true;
            }
        }
        
        /++
         + Returns:
         +  How much time(ms) has passed since the timer was started.
         ++/
        @property
        uint ticks() inout
        {
            if(this._started)
            {
                return (SDL_GetTicks() - this._startTime);
            }
            else
            {
                return (this._stoppedTime - this._startTime);
            }
        }

        /// Ditto
        alias asMilliseconds = ticks;

        /// Gets how much time has passed in seconds
        @property
        float asSeconds() inout
        {
            return (cast(float)this.asMilliseconds / 1000f);
        }
        
        /// Get whether the timer has been started or not.
        @property
        bool started() inout
        {
            return this._started;
        }
    }
}