﻿module jengine.core.world;

private
{
    import jengine.core.allocators, jengine.core.component, jengine.core.sdl;
    import jengine.graphics.renderer, jengine.graphics.texture, jengine.graphics.window;
}

/++
 + A static class representing the physical world.
 + Any PhysicalComponent registered in the world will automatically be updated/onEvented, etc.
 + 
 + This is only really intended for the main state of the game. For things like GUI and stuff, just use a ComponentManager
 + ++/
static class PhysicalWorld
{
    alias Manager = ComponentManager!(Allocators.DefaultT, 5);

    private static
    {
        Manager     _manager;
        Vector2f    _camera;
        uint        _amountToRemove;

        void updateComponent(Component c, ref float time)
        {
            auto comp = cast(PhysicalComponent)c;
            assert(comp !is null);

            // Update the component, then draw it.
            comp.onUpdate(time);

            Renderer.drawSprite(comp.sprite);
        }

        void deleteComponent(Component c, ref float time)
        {
            if(c is null)
                return;

            auto comp = (cast(PhysicalComponent)c);
            assert(comp !is null);

            if(comp.shouldRemove)
            {
                this._manager.remove(comp);
                this._amountToRemove -= 1;

                if(this._amountToRemove == 0)
                    this._manager.stopTask();
            }
        }
    }

    public static
    {
        /++
         + Sets up the PhysicalWorld.
         + ++/
        void setup()
        {
            this._manager = theAllocator.make!Manager(Allocators.Default);
        }

        /++
         + Destroys the world.
         + ++/
        void destroy()
        {
            if(this._manager !is null)
            {
                theAllocator.dispose(this._manager);
                this._manager = null;
            }
        }

        /++
         + Clears the world of all entities, also freeing any unused space.
         + ++/
        void clear()
        {
            PhysicalWorld.destroy();
            PhysicalWorld.setup();
        }

        /++
         + Adds a physical component to the world.
         + 
         + Asserts:
         +  $(B component) must not be null.
         + 
         + Parameters:
         +  component   = The component to add.
         +  name        = The name to give the component.
         + ++/
        void add(PhysicalComponent component, string name = "")
        {
            assert(component !is null, "Cannot add a null component to the world.");
            component.name = name;

            // Just incase the component already exists, remove it.
            this._manager.remove(component);
            this._manager.put(component);
        }

        /++
         + Updates all components in the world.
         + 
         + Every component is checked to see if it colloding with another component, with onHitEntity being called for
         + both components if they're colliding.
         + 
         + Parameters:
         +  gameTime = How much time, in seconds, the last frame took.
         + ++/
        void onUpdate(ref float gameTime)
        {
            import std.functional : toDelegate;
            this._manager.doTask(gameTime, toDelegate(&PhysicalWorld.updateComponent));

            // TODO: Optimise this.
            auto entities = this._manager.components;
            foreach(i; 0..entities.length)
            {
                auto first = cast(PhysicalComponent)entities[i];
                assert(first !is null);

                foreach(k; i+1..entities.length)
                {
                    auto second = cast(PhysicalComponent)entities[k];
                    assert(second !is null);

                    //writefln("i = %s | k = %s | collide = %s | 1 = %s | 2 = %s", i, k, first.sprite.bounds.collides(second.sprite.bounds), first.sprite.bounds, second.sprite.bounds);
                    if(first.sprite.collides(second.sprite))
                    {
                        first.onHitEntity(second);
                        second.onHitEntity(first);
                    }
                }
            }

            this._manager.doTask(gameTime, toDelegate(&PhysicalWorld.deleteComponent));
        }

        /++
         + Attempts to find a component in the world.
         + 
         + Parameters:
         +  name = The name of the component to find.
         + 
         + Returns:
         +  The component.
         + ++/
        PhysicalComponent find(string name)
        {
            return this._manager.find!PhysicalComponent(name);
        }

        /// Ditto
        T findAs(T : PhysicalComponent)(string name)
        {
            return cast(T)PhysicalWorld.find(name);
        }

        /++
         + Calls onEvent for each component in the world.
         + 
         + Parameters:
         +  event   = The event to process.
         +  handled = Has the event already been handled?
         + 
         + Returns:
         +  $(D true) if $(B event) was handled. $(D false) otherwise.
         + ++/
        bool onEvent(SDL_Event e, bool handled)
        {
            return this._manager.onEvent(e, handled);
        }

        /++
         + Marks a physical component to be removed from the world.
         + 
         + Asserts:
         +  $(B component) must not be null.
         + 
         + Parameters:
         +  component = The component to remove.
         + ++/
        void remove(PhysicalComponent component)
        {
            assert(component !is null);

            component.shouldRemove   = true;
            this._amountToRemove    += 1;
        }
    }
}

/++
 + Any component who's main purpose is to control an on-screen sprite that can interact with other sprites should inherit from this class.
 + ++/
class PhysicalComponent : Component
{
    private
    {
        Sprite      _sprite;
        Animation   _ani;
        bool        _remove;
    }
    
    public
    {
        abstract
        {
            override void onInit()
            {
                assert(this.sprite !is null, "Please setup the component' Sprite. this.sprite = Sprite.make(Cache.loadTexture("")); The Animation class works as well.");
                super.onInit();
            }

             /++
             + Called whenever the physical component overlaps with another.
             + 
             + Parameters:
             +  other = The other entity.
             + ++/
            void onHitEntity(PhysicalComponent other);

            /++
             + Called when any data of the component should be unloaded.
             + ++/
            override void onUninit()
            {
                if(this._sprite !is null)
                    Sprite.dispose(this._sprite);
                else if(this._ani !is null)
                    Animation.dispose(this._ani);

                super.onUninit();
            }
        }

        /++
         + Gets this component's sprite.
         + ++/
        @property @nogc @safe
        inout(Sprite) sprite() nothrow inout
        out(sprit)
        {
            assert(sprit !is null, "The component hasn't given itself a sprite.");
        }
        body
        {
            return (this._ani is null) ? this._sprite : this._ani.sprite;
        }

        /++
         + Gets whether this component's sprite is coming from an Animation class.
         + If this returns $(D true), then you're allowed to make use of PhysicalComponent.animation
         + ++/
        @property @nogc @safe
        inout(bool) usesAnimation() nothrow inout
        {
            return this._ani !is null;
        }

        /++
         + Gets the animation this component uses.
         + ++/
        @property @nogc @safe
        Animation animation() nothrow
        {
            assert(this.usesAnimation, "Please make sure the component is using an Animation before calling this.");
            return this._ani;
        }

        @property @nogc @safe
        ref inout(bool) shouldRemove() nothrow inout
        {
            return this._remove;
        }
    }
    
    protected
    {
        /++
         + Sets the component's sprite
         + ++/
        @property @nogc @safe
        void sprite(Sprite sprit) nothrow
        {
            this._sprite = sprit;
        }

        /// Ditto
        @property @nogc @safe
        void sprite(Animation ani) nothrow
        {
            this._ani = ani;
        }
    }
}