﻿module jengine.core.component;

private
{
    debug import std.format : format;
    import std.range     : isOutputRange;

    import jengine.core.allocators;
    import jengine.graphics.texture : Sprite;
}

public
{
    import derelict.sdl2.sdl;

    import jengine.core.timer;
}

/++
 + Any class that needs to update itself each frame should inherit from this class.
 + ++/
class Component
{
    private
    {
        bool    _hasInit = false;
        string  _name;
    }

    public abstract
    {
        /++
         + Called when any data of the component should be loaded.
         + ++/
        void onInit()
        {
            this._hasInit = true;
        }

        /++
         + Called everytime the component is updated.
         + 
         + Parameters:
         +  gameTime = A reference to the amount of time the last frame took.
         + ++/
        void onUpdate(ref float gameTime);

        /++
         + Called everytime an event is to be processed.
         + 
         + Parameters:
         +  event   = The event to process.
         +  handled = Has the event already been handled?
         + 
         + Returns:
         +  $(D true) if $(B event) was handled. $(D false) otherwise.
         + ++/
        bool onEvent(SDL_Event event, bool handled);

        /++
         + Called when any data of the component should be unloaded.
         + ++/
        void onUninit()
        {
            this._hasInit = false;
        }
    }

    public
    {
        /++
         + Gets whether this component has been initialised.
         + ++/
        @property @nogc @safe
        inout(bool) hasInit() nothrow inout
        {
            return this._hasInit;
        }

        /++
         + Gets the component's name
         + ++/
        @property @nogc @safe
        inout(string) name() nothrow inout
        {
            return this._name;
        }

        /++
         + Sets the component's name.
         + Can only be set once.
         + ++/
        @property @nogc @safe
        void name(string nam) nothrow
        {
            assert(this.name is null || this.name.length == 0, "The name for this component has already been set.");
            this._name = nam;
        }
    }
}

/// The default ComponentManager setup. Because default template parameters still mean I have to at least give one template parameter myself... (bug?)
alias ComponentManagerDefault = ComponentManager!(typeof(Allocators.statMallocator), 1);

/++
 + A class to contain a dynamic array of components.
 + It's a class because I prefer reference types for these kind of things.
 + 
 + Parameters:
 +  Allocator = The allocator to use for the array to store the components.
 +  stepSize = Whenever the buffer's size grows, this is how many extra elements are added to it.
 + ++/
class ComponentManager(Allocator = typeof(Allocators.statMallocator), size_t stepSize = 1)
{
    import std.experimental.allocator : stateSize;
    enum useObject = (stateSize!Allocator != 0);

    private
    {
        Component[] _components;
        size_t      _index;
        bool        _stop;

        static if(useObject)
            Allocator   _alloc;
        else
            alias _alloc = Allocator.instance;

        private void doTask(ref float time, ref int i, void delegate(Component, ref float) task)
        {
            auto before = this.components;          
            foreach(_; i..before.length)
            {
                if(i >= before.length || this._stop)
                    break;
                
                auto comp = before[i];
                task(comp, time);
                i += 1;
                
                // Stops the engine from crashing if _components is reallocated/relocated in memory.
                if(before.ptr != this.components.ptr)
                {
                    if(before.length < this.components.length)
                        doTask(time, i, task);
                    else
                        assert(false, "Please do *not* shrink the buffer via whatever magic you did.");
                }
            }
        }

        private void doUpdate(Component c, ref float time)
        {
            c.onUpdate(time);
        }
    }

    public
    {
        // If we need an object, create a constructor for it.
        static if(useObject)
        {
            /++
             + Creates the ComponentManager.
             + 
             + Asserts:
             +  $(B object) must not be null.
             + 
             + Parameters:
             +  object = The allocator object to use.
             + ++/
            @nogc @safe
            this(Allocator object) nothrow
            {
                assert(object !is null, "The allocator must not be null.");
                this._alloc = object;
            }
        }

        /++
         + If the Allocator passed uses the GC then.... Good luck with whatever happens
         + ++/
        ~this()
        {
            import std.stdio;
            import std.algorithm : each;

            if(this._components is null)
                return;
                
            // For some reason, lambdas in the form of "(c) => {c.x(); c.y();}" just do not work with std.algorithm.each
            void disposeOf(Component c) {c.onUninit(); this._alloc.dispose(c);}
            this.components.each!disposeOf;

            this._alloc.dispose(this._components);
        }

        /++
         + Adds a component into the manager.
         + The component's onInit function will be called if hasInit is false.
         + Please note that this may cause a reallocation to take place, so any references may become invalid.
         + 
         + Asserts:
         +  If $(B c).hasInit is false after a call to $(B c).onInit, then it is deemed a bug.
         +  $(B c) must not be null.
         + 
         + Parameters:
         +  c = The component to add.
         + ++/
        void put(Component c)
        {
            assert(c !is null, "The component must not be null.");

            // Grow the array if needed.
            if(this._index >= this._components.length)
                this._alloc.expandArray(this._components, stepSize);

            if(!c.hasInit)
                c.onInit();
                
            assert(c.hasInit, "The component is reporting that it hasn't been initialised, even after a call to onInit. Forget about super.onInit?");

            this._components[this._index++] = c;
        }

        /++
         + Removes a certain component from the manager, if it even exists.
         + Note, the component is assumed to be made with the same allocator given to the manager.
         + 
         + Asserts:
         +  $(B c).hasInit must return false after a call to $(B c).onUninit
         + 
         + Parameters:
         +  c = The component to find.
         + ++/
        void remove(Component c)
        {
            if(c is null)
                return;

            // First, find the component, uninit it, and then dispose it.
            size_t index = size_t.max;
            foreach(i; 0..this._index)
            {
                if(this._components[i] == c)
                {
                    c.onUninit();
                    assert(!c.hasInit, "The component is still saying it's initialised, despite a call to onUninit. Forget about super.onUninit?");

                    this._alloc.dispose(c);
                    this._components[i] = null;

                    index = i;
                    break;
                }
            }

            // If the thing wasn't found, just return.
            if(index == size_t.max)
                return;

            // Now, fix up any gaps, and then decrement _index.
            foreach(i; index..this._index)
            {
                auto component = this._components[i];

                if(component !is null)
                    this._components[i - 1] = component;
            }

            this._index -= 1;
            this._components[this._index] = null; // The last element would've been a duplicate of the one before it. So we're just setting it to null.
        }

        /++
         + Finds a component by name.
         + 
         + Parameters:
         +  [T] = The type to convert the component to.
         +  name = The name of the component to find.
         + 
         + Returns:
         +  Null if the component couldn't be converted to T.
         +  Null if no component was found with $(B name).
         +  Otherwise, returns the component.
         + ++/
        T find(T : Component)(string name)
        {
            import std.algorithm : filter;

            auto filt = this.components.filter!((c) => c.name == name);
            return (filt.empty) ? null : cast(T)filt.front;
        }

        /++
         + Performs a task on all components in the manager.
         + 
         + Parameters:
         +  time = The time the last frame took.
         +  task = The task to perform.
         + ++/
        void doTask(ref float time, void delegate(Component c, ref float time) task)
        {
            auto i = 0;
            doTask(time, i, task);
            this._stop = false;
        }

        /++
         + Tells the manager to stop doing the task it was given.
         + ++/
        void stopTask()
        {
            this._stop = true;
        }

        /++
         + Calls onUpdate for each component in the manager, in the order they were added.
         + 
         + Parameters:
         +  gameTime = A reference to the amount of time the last frame took.
         + ++/
        void onUpdate(ref float gameTime)
        {
            auto i = 0;
            doTask(gameTime, i, &doUpdate);
            this._stop = false;
        }

        /++
         + Calls onEvent for each component in the manager, in the order they were added.
         + 
         + Parameters:
         +  event   = The event to process.
         +  handled = Has the event already been handled?
         + 
         + Returns:
         +  $(D true) if $(B event) was handled. $(D false) otherwise.
         + ++/
        bool onEvent(SDL_Event e, bool handled)
        {
            foreach(comp; this.components)
            {
                if(comp.onEvent(e, handled) && !handled)
                    handled = true;
            }

            return handled;
        }

        /++
         + Get all of the components in the manager.
         + ++/
        @property @nogc @safe
        Component[] components() nothrow
        {
            return this._components[0..this._index];
        }
    }
}
unittest
{
    class Dummy : Component
    {
        public
        {
            uint[] tests;
            this(uint[] tests)
            {
                this.tests = tests;
            }
        }

        public override
        {
            void onInit()
            {
                this.tests[0] += 1;
                super.onInit();
            }
            void onUpdate(ref float gameTime)
            {
                this.tests[1] += 1;
            }
            bool onEvent(SDL_Event event, bool handled)
            {
                this.tests[2] += 1;
                return true;
            }
            void onUninit()
            {
                this.tests[3] += 1;
                super.onUninit();
            }
        }
    }

    Allocators.setup();
    theAllocator = Allocators.statMallocator;

    // Just to quickly test it will work with both objects, and things like Mallocator.instance
    auto killMePlease = theAllocator.make!(ComponentManager!Mallocator)();
    theAllocator.dispose(killMePlease);

    uint[] tests = theAllocator.makeArray!uint(4);
    scope(exit) theAllocator.dispose(tests);

    auto manager = theAllocator.make!ComponentManagerDefault(Allocators.statMallocator);
    scope(failure) // manager is deallocated later down. So this is only here in case a unittest fails.
    {
        if(manager !is null)
            theAllocator.dispose(manager);
    }

    manager.put(theAllocator.make!Dummy(tests));
    manager.put(theAllocator.make!Dummy(tests));
    assert(tests[0] == 2);
    assert(&manager.components[0] != &manager.components[1]);

    manager.components[0].name = "Dummy";
    assert(manager.components[0].name == "Dummy");
    assert(manager.find!Dummy("Dummy") == manager.components[0]);
    assert(manager.find!Dummy("Smarty") is null);

    float f = 0;
    manager.onUpdate(f);
    assert(tests[1] == 2);

    assert(manager.onEvent(SDL_Event(), false));
    manager.onEvent(SDL_Event(), false);
    assert(tests[2] == 4);

    assert(tests[3] == 0);
    theAllocator.dispose(manager);
    manager = null;
    assert(tests[3] == 2);

    //Allocators.printStats(); // "bytesUsed" should be at 16, because of the 4 uints still being around.
}

/++
 + This component keeps track of the game's FPS, and reports it to a given output range.
 + 
 + Description:
 +  After 1 second, the calculated FPS is reported using a given Output Range.
 + 
 +  If the Range only supports outputting ushorts, then the FPS is simply sent over as a number.
 +  Otherwise, a string will be put into the range.
 + 
 +  Do note that for now, strings are made using the GC.
 + ++/
class FPSReporter(R) : Component
if(isOutputRange!(R, string) || isOutputRange!(R, ushort))
{
    import std.math   : approxEqual;
    import std.format : format;

    private
    {
        Timer   _timer;
        ushort  _calls;
        R       _range;
    }

    public
    {
        /++
         + Creates an FPSReporter.
         + 
         + Parameters:
         +  range = The output range to use.
         + ++/
        this(R range)
        {
            this._range      = range;
        }

        override
        {
            /++
             + Called when any data of the component should be loaded.
             + ++/
            void onInit()
            {
                this._timer.restart;
                super.onInit();
            }
            
            /++
             + Called everytime the component is updated.
             + 
             + Parameters:
             +  gameTime = A reference to the amount of time the last frame took.
             + ++/
            void onUpdate(ref float gameTime)
            {
                auto elapsed =  this._timer.asSeconds;
                this._calls  += 1;

                // Per-second one.
                if(elapsed.approxEqual(1.0f) || elapsed > 1.0f)
                {
                    static if(isOutputRange!(R, string))
                    {
                        this._range.put(format("%s", this._calls));
                    }
                    else
                    {
                        this._range.put(this._calls);
                    }

                    this._calls = 0;
                    this._timer.restart;
                }
            }
            
            /++
             + Called everytime an event is to be processed.
             + 
             + Parameters:
             +  event   = The event to process.
             +  handled = Has the event already been handled?
             + 
             + Returns:
             +  $(D true) if $(B event) was handled. $(D false) otherwise.
             + ++/
            bool onEvent(SDL_Event event, bool handled)
            {
                return false;
            }
            
            /++
             + Called when any data of the component should be unloaded.
             + ++/
            void onUninit()
            {
                this._calls = 0;
                this._timer.stop;

                super.onInit();
            }
        }
    }
}