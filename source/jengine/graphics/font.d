﻿module jengine.graphics.font;

private
{
    import derelict.sdl2.ttf;

    import jengine.core.sdl, jengine.core.allocators;
    import jengine.graphics.renderer, jengine.graphics.texture;
}

/++
 + A wrapper around a TTF_Font*
 + 
 + TODO: Implement funtions for things like TTF_SetFontHinting
 + ++/
class Font
{
    private
    {
        TTF_Font*   _handle;
    }

    public
    {
        /++
         + Constructs a new font around the given handle.
         + 
         + Parameters:
         +  handle = The handle to wrap around.
         + ++/
        @nogc @safe
        this(TTF_Font* handle) nothrow
        {
            this._handle = handle;
        }

        /++
         + Opens a font from a file.
         + 
         + Parameters:
         +  file = The file that holds the font.
         +  size = The size of the font.
         + 
         + Returns:
         +  The font found in $(B file).
         + ++/
        static Font fromFile(string file, int size)
        {
            enforce(file !is null && file.length != 0, "The file path is either null or empty.");
            
            // Quickly add a null terminator if it doesn't exist.
            bool   dealloc = false;
            char[] buffer;
            if(file[$-1] != '\0')
            {
                buffer          = theAllocator.makeArray!char(file.length + 1);
                buffer[0..$-1]  = file;
                buffer[$-1]     = '\0';
                
                file    = buffer.assumeUnique;
                dealloc = true;
            }
            
            auto handle = TTF_OpenFont(file.ptr, size);
            enforce(handle !is null, new TTFException("Unable to load file at path: " ~ file));
            
            file = null;
            if(dealloc)
                theAllocator.dispose(buffer);
            
            return Allocators.Font.make!Font(handle);
        }

        /++
         + Disposes of the given font.
         + Only call this function if the font was made with Font.fromFile, or Allocators.Font.make
         + 
         + Parameters:
         +  font = The font to dispose of.  
         +         Is set to null afterwards.
         + ++/
        static void dispose(ref Font font)
        {
            if(font is null)
                return;

            TTF_CloseFont(font.handle);
            Allocators.Font.dispose(font);
            font = null;
        }

        /++
         + Get the handle for the font.
         + ++/
        @property @nogc @safe
        inout(TTF_Font*) handle() nothrow inout
        {
            return this._handle;
        }
    }
}

/++
 + A class that contains all the information needed to render text to the screen.
 + 
 + TODO: Add things like different text rendering options, flip options, rotation, etc.
 + ++/
class Text
{
    private
    {
        Font        _font;
        char[]      _text;
        size_t      _index;

        Colour      _colour;
        Vector2f    _position;
        Texture     _texture;

        void updateTexture()
        {
            if(this._texture !is null)
                Texture.dispose(this._texture);

            auto handle   = TTF_RenderText_Blended(this.font.handle, this.text.ptr, this._colour.asSDLColour);
            this._texture = Allocators.Texture.make!Texture(SDL_CreateTextureFromSurface(Renderer.handle, handle));
            
            SDL_FreeSurface(handle);
        }
    }

    public
    {
        /++
         + Creates a new piece of Text.
         + 
         + Parameters:
         +  text        = The inital text to render. May be null.
         +  font        = The font to use to render the text with.
         +  position    = The position to render the text at.
         + ++/
        this(string text, Font font, Vector2f position = Vector2f())
        {
            assert(font !is null, "Cannot use a null font");

            this._font      = font;
            this.text       = text;
            this._colour    = Colour(255, 255, 255);
            this._position  = position;
        }

        /// Ditto
        static Text make(string text, Font font, Vector2f position = Vector2f())
        {
            return Allocators.Text.make!Text(text, font, position);
        }

        /++
         + Disposes the given text.
         + 
         + Parameters:
         +  text = The text to dispose.
         +         Is set to null afterwards.
         + ++/
        static void dispose(ref Text text)
        {
            if(text is null)
                return;

            Allocators.Text.dispose(text._text);
            if(text.texture !is null)
                Texture.dispose(text._texture);

            Allocators.Text.dispose(text);
            text = null;
        }

        /++
         + Sets the text for the Text.
         + ++/
        @property
        void text(const(char)[] text_)
        {
            if(text_ is null)
                return;

            // Grow the text buffer if needed
            if(text_.length > this._text.length)
                Allocators.Text.expandArray(this._text, (text_.length - this._text.length) + 1);

            this._index                = text_.length;
            this._text[0..this._index] = text_[];
            this._text[$-1]            = '\0';

            // Then render the text and convert it into a texture.
            this.updateTexture();
        }

        /++
         + Gets the text of this Text.
         + ++/
        @property @nogc @safe
        const(char)[] text() nothrow const
        {
            return this._text[0..this._index];
        }

        /++
         + Gets the font of the Text.
         + ++/
        @property @nogc @safe
        inout(Font) font() nothrow inout
        {
            return this._font;
        }

        /++
         + Gets the colour of the text.
         + ++/
        @property @nogc @safe
        inout(Colour) colour() nothrow inout
        {
            return this._colour;
        }

        /++
         + Sets the colour of the text.
         + ++/
        @property
        void colour(Colour colour)
        {
            this._colour = colour;
            this.updateTexture();
        }

        /++
         + Gets a reference to the position of the text.
         + ++/
        @property @nogc @safe
        ref inout(Vector2f) position() nothrow inout
        {
            return this._position;
        }

        /++
        + Gets the texture of the Text
        ++/
        @property @nogc @safe
        inout(Texture) texture() nothrow inout
        {
            return this._texture;
        }
    }
}