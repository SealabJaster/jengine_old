﻿module jengine.graphics.camera;

private
{
    import jengine.core.allocators, jengine.core.sdl;
    import jengine.graphics.texture, jengine.graphics.window;
}

/++
 + Cameras can be attached to a sprite to make it be affected by the camera.
 + ++/
class Camera
{
    private
    {
        static Camera _instance;

        Vector2f _position;
    }

    public
    {
        static ~this()
        {
            if(this._instance !is null)
                Allocators.Default.dispose(this._instance);
        }

        /++
         + Get a global instance of a Camera.
         + ++/
        static Camera instance()
        {
            if(this._instance is null)
                this._instance = Allocators.Default.make!Camera;

            return this._instance;
        }

        /++
         + Moves the camera.
         + 
         + Parameters:
         +  offset = The offset to move the camera by.
         + ++/
        @safe @nogc
        void move(const(Vector2f) offset) nothrow
        {
            this._position += offset;
        }
        
        /++
         + Centers the camera to a point.
         + 
         + Parameters:
         +  point = The point to center on
         + ++/
        @trusted @nogc
        void centreOn(const(Vector2f) point) nothrow
        {
            auto size       = Window.size;
            this._position  = point - (size.asVector!float / Vector2f(2.0f, 2.0f));
        }

        /++
         + Centers the camera to the centre of the sprite.
         + 
         + Parameters:
         +  sprite = The sprite to centre on.
         + ++/
        @trusted @nogc
        void centreOn(const(Sprite) sprite) nothrow
        {
            this.centreOn(sprite.position + sprite.centre.asVector!float);
        }
        
        /++
         + Converts a pair of screen-coordiantes to world coordinates.
         + 
         + Parameters:
         +  position = The position to convert.
         + 
         + Returns:
         +  $(B position) as world-coordinates.
         + ++/
        @safe @nogc
        Vector2f toWorldCoords(const(Vector2f) position) nothrow
        {
            return (position + this._position);
        }
        
        /++
         + Converts a pair of world-coordinates to screen-coordinates.
         + 
         + Parameters:
         +  position = The position to convert.
         + 
         + Returns:
         +  $(B position) as screen-coordinates.
         + ++/
        @safe @nogc
        Vector2f toScreenCoords(const(Vector2f) position) nothrow
        {
            return (position - this._position);
        }

        /++
         + Gets a reference to the position of the camera
         + ++/
        @property @safe @nogc
        ref Vector2f position() nothrow
        {
            return this._position;
        }

        /++
         + Get a rectangle representing the view of the camera.
         + ++/
        @property @safe @nogc
        Rectangle box() nothrow
        {
            auto size = Window.size;
            return Rectangle(cast(int)this.position.x, cast(int)this.position.y, size.x, size.y);
        }
    }
}